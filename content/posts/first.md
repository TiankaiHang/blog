---
title: "My first blog post"
date: 2024-05-13
draft: false
ShowToc: true
params:
  math: true
---
Hi!

This is my first blog post. I hope you enjoy it.


## Install Hugo

```bash
wget https://github.com/gohugoio/hugo/releases/download/v0.125.7/hugo_0.125.7_linux-amd64.deb
sudo dpkg -i hugo_0.125.7_linux-amd64.deb

# extended version
wget https://github.com/gohugoio/hugo/releases/download/v0.125.7/hugo_extended_0.125.7_linux-amd64.deb
sudo dpkg -i hugo_extended_0.125.7_linux-amd64.deb

# check version
hugo version
```

Some Icons can be found [here](https://github.com/adityatelange/hugo-PaperMod/wiki/Icons).

## Set Math

ref to [this](https://adityatelange.github.io/hugo-PaperMod/posts/math-typesetting/)

1. create a file at `/layouts/partials/math.html`

```html
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"></script>
<script>
  MathJax = {
    tex: {
      displayMath: [['\\[', '\\]'], ['$$', '$$']],  // block
      inlineMath: [['\\(', '\\)']]                  // inline
    }
  };
</script>
```

2. create a file at `/layouts/partials/extend_head.html`

```html
{{ if or .Params.math .Site.Params.math }}
{{ partial "math.html" . }}
{{ end }}
```

$$y = \sin (x)$$